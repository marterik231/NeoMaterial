$(function () {
             var $win = $(window);

             $win.scroll(function () {
                 if ($win.scrollTop() == 0) {
                     $('.neo-app--bar').addClass('neo-app--bar-transparent');
                 } else if($win.height() + $win.scrollTop() == $(document).height()) {
                    //do Stuff at Bottom
                 } else {
                     $('.neo-app--bar').removeClass('neo-app--bar-transparent');
                 }
             });
         });